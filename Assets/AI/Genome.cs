﻿using System;
using System.Collections.Generic;
public class Genome
{
    public double[] weights;   //一个染色体存储神经网络中所有的权重
    public double fitness;     //适应度
    public int[] splitPoints;

    public Genome(double[] weights, double fitness, int[] splitPoints)
    {
        this.weights = (double[])weights.Clone();
        this.fitness = fitness;
        this.splitPoints = (int[])splitPoints.Clone();
    }
}

