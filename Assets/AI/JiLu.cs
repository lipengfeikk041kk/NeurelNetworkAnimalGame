﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JiLu : MonoBehaviour
{
    public List<Data> weightsA;//动物A
    public List<Data> weightsB;//动物B
    public void jilu(double[] weight,float fen,int dai,bool isA)//记录权重得分代数
    {      
        weightsA = new List<Data>();
        weightsB = new List<Data>();
        if (isA)
        {
           
           weightsA.Add(new Data { weights = weight, dai = dai, defen = fen });
        }
        else
        {

            weightsB.Add(new Data { weights = weight, dai = dai, defen = fen });
        }
    }
    public double[] Getjilu(bool isA)//根据动物种类获取得分最高的权重
    {
        float fen = -9999;
        double[] weights=null;
        if (isA)
        {
            foreach (var item in weightsA)
            {
                if (item.defen>fen)
                {
                    fen = item.defen;
                    weights = item.weights;
                }
            }        
        }
        else
        {
            foreach (var item in weightsB)
            {
                if (item.defen > fen)
                {
                    fen = item.defen;
                    weights = item.weights;
                }
            }        
        }
        return weights;
    }
    public double[] Getjilu(int dai, bool isA)//根据代数获取某种动物的权重
    {
        double[] weights = null;
        if (isA)
        {
            foreach (var item in weightsA)
            {
                if (item.dai==dai)
                {
                    weights = item.weights;
                }
            }
        }
        else
        {
            foreach (var item in weightsB)
            {
                if (item.dai == dai)
                {
                    weights = item.weights;
                }
            }
        }
        return weights;
    }
}
