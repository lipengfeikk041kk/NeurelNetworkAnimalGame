﻿using System;
using System.Collections;
using System.Collections.Generic;
using TinyTeam.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILoading : TTUIPage
{
    public int sceneNum;
    private Image jindu;
    private Text percent;
    float t;
    public UILoading()
        : base(UIType.Normal, UIMode.HideOther, UICollider.None)//构造函数
    {
        uiPath = "UIPrefab/UILoading";//加载界面预制体路径
    }
    public override void Awake(GameObject go)
    {
        jindu = transform.Find("qianjing").GetComponent<Image>();
        percent = transform.Find("percent").GetComponent<Text>();
    }
    public override void Active()
    {
        base.Active();               
        jindu.StartCoroutine(Load());//开启一个协程
    }
    IEnumerator Load()
    {
        while (true)
        {
            jindu.fillAmount += Time.deltaTime / 2f;
            percent.text = Convert.ToDecimal(jindu.fillAmount * 100).ToString("0") + "%";
            if (jindu.fillAmount >= 1)
            {
                jindu.fillAmount = 1;
                SceneManager.LoadScene("2");//异步加载游戏场景 
                jindu.fillAmount = 0;
                ClosePage<UILoading>();
                ShowPage<UIGame>();
                break;
            }
            yield return null;
        }
    }
}
