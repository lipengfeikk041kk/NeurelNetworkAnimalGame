﻿using System.Collections;
using System.Collections.Generic;
using TinyTeam.UI;
using UnityEngine;
using UnityEngine.UI;

public class UILogin : TTUIPage
{
    public UILogin()
        : base(UIType.Normal, UIMode.HideOther, UICollider.None)
    {
        uiPath = "UIPrefab/LonginUI";
    }
    public override void Awake(GameObject go)
    {
        transform.Find("Button").GetComponent<Button>().onClick.AddListener(KaiShi);
        base.Awake(go);
    }
    public void KaiShi()
    {
        TTUIPage.ShowPage<UILoading>();
        Debug.Log(2112121);
    }
}
