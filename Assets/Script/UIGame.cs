﻿using System.Collections;
using System.Collections.Generic;
using TinyTeam.UI;
using UnityEngine;
using UnityEngine.UI;

public class UIGame : TTUIPage
{
    /// <summary>
    /// 饥饿值
    /// </summary>
    public Slider Starvation;
    /// <summary>
    /// 体力值
    /// </summary>
    public Slider Physica;
    public Text fenshu;
    public Text time;
    public UIGame()
        : base(UIType.Normal, UIMode.HideOther, UICollider.None)
    {
        uiPath = "UIPrefab/GameUI";
    }
    public override void Awake(GameObject go)
    {
        Starvation = transform.Find("饥饿值").GetComponent<Slider>();
        Physica = transform.Find("体力值").GetComponent<Slider>();
        fenshu = transform.Find("得分").GetComponent<Text>();
        time = transform.Find("time").GetComponent<Text>();
        transform.Find("加速").GetComponent<Button>().onClick.AddListener(JiaSu);
        base.Awake(go);
    }
    public void JiaSu()
    {

    }
}
