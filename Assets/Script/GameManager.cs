﻿using System.Collections;
using System.Collections.Generic;
using TinyTeam.UI;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        TTUIPage.ShowPage<UILogin>();
        DontDestroyOnLoad(TTUIRoot.Instance.gameObject);//保持根界面在加载时不会被删除       
    }

    // Update is called once per frame
    void Update()
    {

    }
}
