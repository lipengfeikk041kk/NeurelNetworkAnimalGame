﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtrl : MonoBehaviour
{
    Vector3 v3;
    Vector3 v4;
    public Rigidbody ri;
    public float speed;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (MyJoystick.instance != null && MyJoystick.instance.direction != Vector2.zero)
        {
            v3 = new Vector3(MyJoystick.instance.direction.x, 0, MyJoystick.instance.direction.y);
            v4 = new Vector3(v3.x, 0, v3.z).normalized;
            transform.rotation = Quaternion.LookRotation(v4);
            ri.position = transform.position + v4 * Time.deltaTime * speed;
        }
    }
}
