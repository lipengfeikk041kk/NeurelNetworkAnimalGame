﻿using System.Collections;
using System.Collections.Generic;
using TinyTeam.UI;
using UnityEngine;
using UnityEngine.UI;

public class UIThree : TTUIPage
{
    public Text aMax;
    public Text aPax;
    public Text bMax;
    public Text bPax;

    public UIThree()
        : base(UIType.Normal, UIMode.DoNothing, UICollider.None)//构造函数
    {
        uiPath = "UIPrefab/UIThree";//加载界面预制体路径
    }
    public override void Awake(GameObject go)
    {
        aMax = transform.Find("Afen").GetComponent<Text>();
        aPax = transform.Find("APfen").GetComponent<Text>();
        bMax = transform.Find("Bfen").GetComponent<Text>();
        bPax = transform.Find("BPfen").GetComponent<Text>();
    }

}