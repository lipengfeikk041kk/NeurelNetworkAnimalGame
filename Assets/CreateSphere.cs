﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateSphere : MonoBehaviour
{
    /// <summary>
    /// 生成球球数量
    /// </summary>
    public int Num;
    /// <summary>
    /// 生成球的范围
    /// </summary>
    public float length;

    public GameObject qiu;

    
    // Use this for initialization
    void Start()
    {
       
        for (int i = 1; i <= Num; i++)
        {
           float rd1 = Random.Range(0, length);
           print(rd1);
           float rd2 = Random.Range(0, length);
           print(rd2);
           GameObject Q = Instantiate(qiu);
           Q.transform.position = new Vector3(rd1, 10, rd2);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
