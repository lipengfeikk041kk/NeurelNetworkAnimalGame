﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinOrDefeated : MonoBehaviour
{
    public static WinOrDefeated instance;
    void Awake()
    {
        instance = this;
    }
    /// <summary>
    /// 胜利之后，获取本代食草动物权重，进行杂交。返回新一代的权重
    /// </summary>
    /// <param name="Pop">下一代种群大小</param>
    /// <param name="parents">本代种群的全部权重</param>
    public List<double[]> WhenWin_CreateBaby(int Pop, List<Genome> parents)
    {
        GA newGA = new GA(Pop);
        return newGA.Run(parents);
    }



    /// <summary>
    /// 根据场景名称加载场景
    /// </summary>
    /// <param name="sceneName">要加载的场景名称</param>
    public void WinOrDefeated_LoadScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }
    //重载：
    /// <summary>
    /// 根据场景下标加载场景
    /// </summary>
    /// <param name="index">要加载的场景下标</param>
    public void WinOrDefeated_LoadScene(int index)
    {
        SceneManager.LoadSceneAsync(index);
    }
}
