﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ACtrl : MonoBehaviour
{
    public Genome genome;
    public NeuralNetwork nn;

    public int score;

    public Rigidbody rb;

    GameObject target;
    GameObject enemy;
    float temp;
    void Awake()
    {
        //神经网络形状
        nn = new NeuralNetwork(new int[] { 6, 2 });
        //随机权重
        nn.RandomWeights();
        //生成基因
        genome = new Genome(nn.GetWeights(), 0, nn.splitPoints);
    }

    void Update()
    {
        //找到最近的草
        temp = 9999999;
        GameObject[] allCao = GameObject.FindGameObjectsWithTag("cao");
        foreach (var item in allCao)
        {
            float dis = Vector3.Distance(transform.position, item.transform.position);
            if (temp >= dis)
            {
                target = item;
            }
        }
        //找到最近的敌人
        temp = 99999999;
        GameObject[] allEnemy = GameObject.FindGameObjectsWithTag("enemy");
        foreach (var item in allEnemy)
        {
            float dis = Vector3.Distance(transform.position, item.transform.position);
            if (temp >= dis)
            {
                enemy = item;
            }
        }
        //获取结果
        double[] res = nn.Run(new double[] {transform.position.x ,transform.position.z,target.transform.position.x, target.transform.position.z, enemy.transform.position.x, enemy.transform.position.z });
        //移动
        rb.velocity = transform.forward *20* (float)res[0];
        transform.Rotate(Vector3.up, 10 * (float)res[1]);
    }
}
